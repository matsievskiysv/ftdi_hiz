#include <docopt.h>
#include <ftdi.h>
#include <iostream>
#include <map>
#include <string>

const char USAGE[] =
    R"(FTDI HIZ.
Set FTDI outputs to HiZ.

    Usage:
      beadpullxx --vid <vid> --pid <pid>
      beadpullxx (-h | --help)
      beadpullxx (-v | --version)

    Options:
      -h --help          Show this screen.
      -v --version       Show version.
      --vid=<vid>        USB device vendor ID.
      --pid=<pid>        USB device product ID.
)";

class FTDI
{
      private:
	struct ftdi_context ftdi;

      public:
	//! Default constructor
	FTDI(size_t vid, size_t pid)
	{
		if (ftdi_init(&ftdi) < 0)
			throw std::runtime_error("FTDI initialization error");
		if (ftdi_usb_open(&ftdi, vid, pid))
			throw std::runtime_error("cannot open USB device");
	}

	void
	set_hiz()
	{
		for (int interface = INTERFACE_A; interface <= INTERFACE_D; ++interface) {
			if (ftdi_set_interface(&ftdi, static_cast<enum ftdi_interface>(interface)))
				throw std::runtime_error("cannot select interface");
			ftdi_set_bitmode(&ftdi, 0x00, BITMODE_RESET);
			ftdi_set_bitmode(&ftdi, 0x00, BITMODE_BITBANG);
		}
	}

	//! Copy constructor
	FTDI(const FTDI &other) = delete;

	//! Move constructor
	FTDI(FTDI &&other) noexcept = delete;

	//! Destructor
	virtual ~FTDI() noexcept
	{
		ftdi_usb_close(&ftdi);
		ftdi_deinit(&ftdi);
	};

	//! Copy assignment operator
	FTDI &operator=(const FTDI &other) = delete;

	//! Move assignment operator
	FTDI &operator=(FTDI &&other) noexcept = delete;
};

int
main(int argc, char *argv[])
{
	int				     vid, pid;
	std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {argv + 1, argv + argc}, true, "FTDI HIZ");

	vid = stoi(args.at("--vid").asString(), nullptr, 0);
	pid = stoi(args.at("--pid").asString(), nullptr, 0);

	FTDI ftdi(vid, pid);
	ftdi.set_hiz();

	return 0;
}
