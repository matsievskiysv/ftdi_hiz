cmake_minimum_required(VERSION 3.17 FATAL_ERROR)

project(ftdi_hiz LANGUAGES C CXX VERSION 0.1)
set(CMAKE_CXX_STANDARD 20)
add_compile_options(-Wall -Wextra -Wunreachable-code -Wpedantic -Werror)

set(DEFAULT_BUILD_TYPE "Release")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${DEFAULT_BUILD_TYPE}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${DEFAULT_BUILD_TYPE}" CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release")
endif()

# Append to debug version
set(CMAKE_DEBUG_POSTFIX "_debug")

find_package(PkgConfig REQUIRED)
find_package(docopt COMPONENTS CXX REQUIRED)
pkg_check_modules(FTDI REQUIRED libftdi)

add_executable(ftdi_hiz)
target_sources(ftdi_hiz PRIVATE main.cxx)
# target_compile_definitions(usockets PRIVATE LIBUS_USE_OPENSSL)
target_include_directories(ftdi_hiz PUBLIC ${FTDI_INCLUDE_DIRS} docopt)
target_compile_options(ftdi_hiz PRIVATE ${FTDI_CFLAGS})
target_link_libraries(ftdi_hiz PUBLIC ${FTDI_LIBRARIES} docopt)
